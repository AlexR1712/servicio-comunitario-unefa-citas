<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('estudiante.create');
});
Route::get('/planilla', function () {
    return view('estudiante.consulta_planilla');
})->name('planilla');

Route::get('/planilla/{id}', 'EstudiantesController@pdf')->name('planilla.pdf');
Route::post('/planilla/get', 'EstudiantesController@search')->name('planilla.search');

Route::get('/contacto', 'MensajesController@index')->name('contacto');
Route::post('/contacto', 'MensajesController@store')->name('contacto.store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/proyectos', 'ProyectosController@index')->name('proyectos');
Route::get('/proyectos/create', 'ProyectosController@create')->name('proyectos_create');
Route::get('/proyectos/{id}', 'ProyectosController@edit')->name('proyectos_editar');
Route::get('/proyectos/delete/{id}', 'ProyectosController@destroy')->name('proyectos_delete');
Route::post('/proyectos', 'ProyectosController@store')->name('proyectos_store');
Route::put('/proyectos/{id}', 'ProyectosController@update')->name('proyectos_editar_actualizar');

Route::post('/grupos/add', 'GruposController@add')->name('grupo.add');
Route::post('/grupos/set/tutor', 'GruposController@setTutor')->name('grupo.set.tutor');
Route::get('/grupos/delete/{estudiante_id}/{proyecto_id}', 'GruposController@delete')->name('grupo.delete');
Route::get('/mis-grupos', 'GruposController@misGrupos')->name('personal.grupos.list');

Route::get('/estudiantes', 'EstudiantesController@index')->name('estudiantes');
Route::get('/estudiantes/consulta', 'EstudiantesController@get')->name('estudiantes.get');
Route::get('/estudiantes/search', 'EstudiantesController@searchByCedula')->name('estudiantes_search');
Route::get('/estudiantes/{id}', 'EstudiantesController@edit')->name('estudiantes_edit');
Route::get('/estudiantes/delete/{id}', 'EstudiantesController@delete')->name('estudiantes_delete');
Route::put('/estudiantes/{id}', 'EstudiantesController@update')->name('estudiantes_editar_actualizar');
Route::post('/estudiantes', 'EstudiantesController@store')->name('estudiantes_store');

Route::get('/personal', 'PersonalController@index')->name('personal');
Route::get('/personal/create', 'PersonalController@create')->name('personal_create');
Route::get('/personal/get', 'PersonalController@getTutores')->name('tutores.get');

Route::get('/personal/{id}', 'PersonalController@edit')->name('personal_editar');
Route::get('/personal/delete/{id}', 'PersonalController@destroy')->name('personal_delete');
Route::post('/personal', 'PersonalController@store')->name('personal_store');
Route::put('/personal/{id}', 'PersonalController@update')->name('personal_editar_actualizar');

Route::get('/mensajes', 'MensajesController@list')->name('mensajes.list');
Route::get('/mensajes/{id}', 'MensajesController@show')->name('mensajes.show');
Route::get('/mensajes/delete/{id}', 'MensajesController@destroy')->name('mensajes.destroy');

Route::get('/citas', 'CitasController@index')->name('citas.create');

Route::get('/panel/citas', 'CitasController@list')->name('citas.list');
Route::post('/citas', 'CitasController@store')->name('citas.store');

Route::get('/mi-horario', 'HorariosController@miHorario')->name('personal.horario.show');
Route::get('/horarios', 'HorariosController@index')->name('horarios');
Route::get('/horarios/get', 'HorariosController@get')->name('horarios.get');
Route::get('/horarios/show', 'HorariosController@show')->name('horarios_show');
Route::post('/horarios', 'HorariosController@store')->name('horarios_store');
Route::get('add-to-log', 'BitacoraCotroller@myTestAddToLog');
Route::get('logActivity', 'BitacoraController@logActivity');
Route::get('mantenimiento', 'MantenimientoController@index')->name('mantenimiento.get');
Route::post('mantenimiento/backup', 'MantenimientoController@backupDataBase')->name('backup.make');
Route::get('mantenimiento/backup/restaurar/{ruta}', 'MantenimientoController@restaurar')->name('backup.restaurar');
