<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    protected $fillable = ['estudiante_id', 'horario_id', 'asunto', 'dia', 'estado'];
}
