<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Backup extends Model
{
    protected $connection = 'mysql2';
    protected $fillable = ['descripcion', 'ruta'];
}
