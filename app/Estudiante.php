<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    protected $fillable = ['nombres', 'apellidos', 'cedula', 'carrera', 'semestre', 'seccion', 'telefono', 'correo', 'direccion', 'turno', 'periodo'];
}
