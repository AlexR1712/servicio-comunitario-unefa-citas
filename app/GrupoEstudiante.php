<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupoEstudiante extends Model
{
    protected $table = 'grupo_estudiante';
    protected $fillable = ['grupo_id', 'estudiante_id'];
}
