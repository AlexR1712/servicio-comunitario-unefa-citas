<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'bitacora';
    protected $fillable = [
        'modulo', 'accion', 'descripcion', 'ip', 'agente', 'propiedades', 'user_id',
    ];
}
