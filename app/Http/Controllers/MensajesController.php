<?php

namespace App\Http\Controllers;

use App\Mensaje;
use Illuminate\Http\Request;

class MensajesController extends Controller
{
    public function index()
    {
        return view('contacto.index');
    }

    public function store(Request $request)
    {
        $request->validate([
            'asunto'      => 'required|max:255',
            'mensaje'     => 'required|max:8000',
            'email'       => 'required|email',
        ]);

        \LogActivity::addToLog([
            'modulo'        => 'mensajes',
            'accion'        => 'insertar',
            'descripcion'   => 'Se ha insertado un nuevo mensaje',
        ]);

        Mensaje::create($request->all());

        return redirect()->back()->with('success', 'Mensaje Enviado Exitosamente!');
    }

    public function list()
    {
        \LogActivity::addToLog([
            'modulo'        => 'mensajes',
            'accion'        => 'listar',
            'descripcion'   => 'Se ha listado los mensajes',
        ]);
        $mensajes = Mensaje::orderBy('created_at', 'DESC')->get();

        return view('contacto.list', ['mensajes' => $mensajes]);
    }

    public function show($id)
    {
        \LogActivity::addToLog([
            'modulo'        => 'mensajes',
            'accion'        => 'visualizar',
            'descripcion'   => 'Se ha visualizado un mensaje',
        ]);
        $mensaje = Mensaje::find($id);

        return view('contacto.show', ['mensaje' => $mensaje]);
    }

    public function destroy($id)
    {
        $mensaje = Mensaje::find($id);
        \LogActivity::addToLog([
            'modulo'        => 'mensajes',
            'accion'        => 'eliminar',
            'descripcion'   => 'Se ha eliminado un mensaje',
        ], $mensaje->toArray());
        $mensaje->delete();

        return redirect()->route('mensajes.list')->with('success', 'Mensaje Eliminado');
    }
}
