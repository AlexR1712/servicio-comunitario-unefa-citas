<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \LogActivity::addToLog([
            'modulo'        => 'panel',
            'accion'        => 'visualizar',
            'descripcion'   => 'Se ha visualizado el panel principal',
        ]);

        return view('home');
    }
}
