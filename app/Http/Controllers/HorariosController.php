<?php

namespace App\Http\Controllers;

use App\Estudiante;
use App\Horario;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HorariosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['miHorario', 'store']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \LogActivity::addToLog([
            'modulo'        => 'horarios',
            'accion'        => 'listar',
            'descripcion'   => 'Se ha listado los horarios',
        ]);

        return view('home');
    }

    public function get(Request $request)
    {
        $request->validate([
            'tipo'   => 'required',
            'fecha'  => 'required',
            'cedula' => 'required',
        ]);

        try {
            $dt = Carbon::parse($request->fecha);

            $semana = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];

            $result = [];
            if ($request->tipo === 'COORDINADOR') {
                $result['persona'] = User::where('tipo', 'COORDINADOR')->select('name', 'id')->first();
                if ($result['persona'] == null) {
                    return response([
                    'ok'      => false,
                    'mensaje' => 'No existen coordinadores de Servicio Comunitario en este momento',
                ], 403);
                }
            } else {
                $result['persona'] = Estudiante::where('estudiantes.cedula', $request->cedula)
                                ->join('grupo_estudiante', 'estudiantes.id', '=', 'grupo_estudiante.estudiante_id')
                                ->join('grupos', 'grupo_estudiante.grupo_id', '=', 'grupos.id')
                                ->join('users', 'grupos.tutor_id', '=', 'users.id')
                                ->select('users.name', 'users.id')
                                ->first();
                if ($result['persona'] == null) {
                    return response([
                    'ok'      => false,
                    'mensaje' => 'Usted no posee tutor',
                ], 403);
                }
            }

            $horarios = Horario::where([
            'user_id' => $result['persona']->id,
            'dia'     => $dt->dayOfWeek,
        ])->orderBy('dia', 'ASC')->get();

            if (!$horarios) {
                return response('No posee horario disponible', 419)
                ->header('Content-Type', 'application/json');
            }

            foreach ($horarios as $item) {
                if (!isset($result['horario'])) {
                    $result['horario'] = [];
                }
                array_push($result['horario'], [
                'id'     => $item->id,
                'dia'    => $semana[$item->dia],
                'inicio' => date('g:i a', strtotime($item->hora_inicio)),
                'fin'    => date('g:i a', strtotime($item->hora_fin)),
            ]);
            }
        } catch (\Illuminate\Database\QueryException $e) {
            $mensaje = 'Debe restaurar la base de datos en un punto consistente debido al error siguiente: ';

            return redirect()->route('mantenimiento.get')->with('error', $mensaje.$e->getMessage());
        }

        return $result;
    }

    public function miHorario()
    {
        return view('tutor.horario');
    }

    public function store(Request $request)
    {
        // Elimino los registros anteriores
        $request->validate([
            'horarios' => 'required',
            'user_id'  => 'required',
        ]);
        Horario::where('user_id', $request->user_id)->delete();

        $horarios = [];
        foreach ($request->horarios as $dia => $intervalos) {
            foreach ($intervalos as $tiempos) {
                $horarios[] = [
                    'user_id'     => $request->user_id,
                    'dia'         => $dia,
                    'hora_inicio' => $tiempos[0],
                    'hora_fin'    => $tiempos[1],
                    'created_at'  => date('Y-m-d h:i:s'),
                    'updated_at'  => date('Y-m-d h:i:s'),
                ];
            }
        }
        Horario::insert($horarios);
        \LogActivity::addToLog([
            'modulo'        => 'horarios',
            'accion'        => 'insertar',
            'descripcion'   => 'Se ha insertado un nuevo horario',
        ]);

        return $horarios;
    }

    public function show(Request $request)
    {
        $request->validate([
           'user_id' => 'required',
        ]);
        $horarios = Horario::where('user_id', $request->user_id)->get();

        if (!$horarios) {
            return response('usuario_no_encontrado', 419)
            ->header('Content-Type', 'application/json');
        }

        $result = [];

        foreach ($horarios as $item) {
            if (!isset($result[$item->dia])) {
                $result[$item->dia] = [];
            }
            array_push($result[$item->dia], [$item->hora_inicio, $item->hora_fin]);
        }

        return json_encode($result, JSON_FORCE_OBJECT);
    }
}
