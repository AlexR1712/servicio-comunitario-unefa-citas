<?php

namespace App\Http\Controllers;

use App\Cita;
use App\Estudiante;
use App\Horario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CitasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('auth', ['only' => ['list', 'delete']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('citas.create');
    }

    public function list()
    {
        $horarios = Horario::where('user_id', Auth::id())->select('id')->get()->toArray();
        $citas = Cita::whereIn('horario_id', $horarios)
                    ->join('estudiantes', 'citas.estudiante_id', '=', 'estudiantes.id')
                    ->join('horarios', 'citas.horario_id', '=', 'horarios.id')
                    ->select(
                        'estudiantes.nombres',
                        'estudiantes.apellidos',
                        'estudiantes.cedula',
                        'estudiantes.telefono',
                        'estudiantes.correo',
                        'citas.asunto',
                        'citas.dia',
                        'citas.created_at',
                        'horarios.hora_inicio',
                        'horarios.hora_fin')
                    ->get();
        \LogActivity::addToLog([
            'modulo'        => 'citas',
            'accion'        => 'listar',
            'descripcion'   => 'Se ha listado las citas',
        ]);

        return view('citas.index', ['citas' => $citas]);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'cedula'      => 'required|numeric|max:40000000',
            'asunto'      => 'required|max:255',
            'hora'        => 'required|numeric',
        ]);
        $estudiante = Estudiante::where('cedula', $request->cedula)->first();
        $cita = new Cita();
        $cita->estudiante_id = $estudiante->id;
        $cita->horario_id = $request->hora;
        $cita->asunto = $request->asunto;
        $cita->dia = $request->fecha;
        $cita->save();

        \LogActivity::addToLog([
            'modulo'        => 'citas',
            'accion'        => 'insertar',
            'descripcion'   => 'Se ha insertado una nueva cita',
        ]);

        return redirect()->back()->with('success', 'Se ha agendado una Cita Exitosamente!');
    }
}
