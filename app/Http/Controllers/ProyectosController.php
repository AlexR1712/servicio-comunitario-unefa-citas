<?php

namespace App\Http\Controllers;

use App\Grupo;
use App\GrupoEstudiante;
use App\Proyecto;
use App\User;
use Illuminate\Http\Request;

class ProyectosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyectos = Proyecto::orderBy('created_at', 'desc')->get();
        \LogActivity::addToLog([
            'modulo'        => 'proyectos',
            'accion'        => 'listar',
            'descripcion'   => 'Se ha listado los Proyects',
        ]);

        return view('proyecto.index', ['proyectos' => $proyectos]);
    }

    public function edit($id)
    {
        try {
            $proyecto = Proyecto::find($id);
            $grupo = Grupo::where('proyecto_id', $id)
                        ->join('grupo_estudiante', 'grupos.id', '=', 'grupo_estudiante.grupo_id')
                        ->join('estudiantes', 'grupo_estudiante.estudiante_id', '=', 'estudiantes.id')
                        ->select('estudiantes.*', 'grupos.id as grupo_id', 'grupos.tutor_id as tutor_id')
                        ->get();
        } catch (\Illuminate\Database\QueryException $e) {
            $mensaje = 'Debe restaurar la base de datos en un punto consistente debido al error siguiente: ';

            return redirect()->route('mantenimiento.get')->with('error', $mensaje.$e->getMessage());
        }

        // me falta buscar el tutor para colocarlo en la vista y luego asi le das con la cita
        $tutor = null;
        if ($grupo->first()) {
            try {
                $tutor = User::find($grupo->first()->tutor_id);
            } catch (\Illuminate\Database\QueryException $e) {
                $mensaje = 'Debe restaurar la base de datos en un punto consistente debido al error siguiente: ';

                return redirect()->route('mantenimiento.get')->with('error', $mensaje.$e->getMessage());
            }
        }

        // dd(, $grupo);
        \LogActivity::addToLog([
            'modulo'        => 'proyectos',
            'accion'        => 'visualizar',
            'descripcion'   => 'Se ha visualizado el editor de Proyecto',
        ], $proyecto->toArray());

        return view('proyecto.edit', ['proyecto' => $proyecto, 'grupo' => $grupo, 'tutor' => $tutor]);
    }

    public function update($id, Request $request)
    {
        $data = [
            'nombre'    => $request->nombre,
            'status'    => $request->status,
            'lugar'     => $request->lugar,
            'direccion' => $request->direccion,
        ];
        $proyecto = Proyecto::find($id)
                    ->update($data);
        \LogActivity::addToLog([
            'modulo'        => 'proyectos',
            'accion'        => 'editar',
            'descripcion'   => 'Se ha editado el Proyecto '.$request->nombre,
        ], $data);

        return redirect()->back()->with('success', 'Proyecto Actualizado');
    }

    public function create()
    {
        \LogActivity::addToLog([
            'modulo'        => 'proyectos',
            'accion'        => 'crear',
            'descripcion'   => 'Se ingresado al formualrio de creacion de proyecto',
        ]);

        return view('proyecto.create');
    }

    public function store(Request $request)
    {
        $proyecto = new Proyecto();
        $proyecto->nombre = $request->nombre;
        $proyecto->status = $request->status;
        $proyecto->lugar = $request->lugar;
        $proyecto->direccion = $request->direccion;
        $proyecto->save();

        \LogActivity::addToLog([
            'modulo'        => 'proyectos',
            'accion'        => 'insertar',
            'descripcion'   => 'Se ha insertado un nuevo proyecto',
        ], [
            'nombre'    => $request->nombre,
            'status'    => $request->lugar,
            'direccion' => $request->direccion,
        ]);

        return redirect()->route('proyectos_editar', ['id' => $proyecto->id])->with('success', 'Proyecto Creado exitosamente');
    }

    public function destroy($id)
    {
        $proyecto = Proyecto::find($id);
        \LogActivity::addToLog([
            'modulo'        => 'proyectos',
            'accion'        => 'eliminar',
            'descripcion'   => 'Se ha eliminado un proyecto',
        ], $proyecto->toArray());

        $grupo = Grupo::where('proyecto_id', $proyecto->id)->first();
        if ($grupo->id) {
            GrupoEstudiante::where('grupo_id', $grupo->id)->delete();
        }
        $proyecto->delete();

        return redirect()->route('proyectos')->with('info', 'Proyecto Eliminado Exitosamente');
    }
}
