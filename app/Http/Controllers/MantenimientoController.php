<?php

namespace App\Http\Controllers;

use App\Backup;
use DB;
use Illuminate\Http\Request;

class MantenimientoController extends Controller
{
    public function index()
    {
        $backups = Backup::orderBy('created_at', 'DESC')->get();

        return view('mantenimiento.index', ['backups' => $backups]);
    }

    public function restaurar($ruta)
    {
        $tables = DB::select('SHOW TABLES');
        foreach ($tables as $table) {
            //dd($table->Tables_in_sc);
            $droplist[] = $table->Tables_in_sc;
        }
        if (isset($droplist)) {
            $droplist = implode(',', $droplist);
            DB::beginTransaction();
            // Apagar integridad referencial
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
            DB::statement("DROP TABLE $droplist");
            // Encender integridad referencial
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');
            DB::commit();
        }

        $mysql = config('backup.MYSQL_PATH');
        $user = env('DB_USERNAME');
        $pass = env('DB_PASSWORD');
        $db = env('DB_DATABASE');
        $host = env('DB_HOST');

        // mysql -u username -p -h localhost DATA-BASE-NAME < data.sql
        $comando = "$mysql  --user=$user --password=\"$pass\" --host=$host $db < ".base_path('/backups/'.$ruta);
        exec($comando, $output, $return_var);
        if ($return_var == 0) {
            return redirect()->back()->with('success', 'Backup Restaurado');
        }

        return redirect()->back()->with('error', 'No se puedo ejecutar la restauracion..');
    }

    public function backupDataBase(Request $request)
    {
        $request->validate([
            'descripcion'       => 'required|max:255',
        ]);
        $mysqldump = config('backup.MYSQL_DUMP_PATH');
        $user = env('DB_USERNAME');
        $pass = env('DB_PASSWORD');
        $db = env('DB_DATABASE');
        $host = env('DB_HOST');
        $file = 'backup.'.date('Y.m.d.h.i.s').'.sql';
        $comando = "$mysqldump  --user=$user --password=\"$pass\" --host=$host $db > ".base_path('/backups/'.$file);
        exec($comando, $output, $return_var);

        if ($return_var == 0) {
            Backup::create([
                'descripcion' => $request->descripcion,
                'ruta'        => $file,
            ]);

            return redirect()->back()->with('success', 'Backup Generado');
        }

        return redirect()->back()->with('error', 'Ocurrio un problema al ejecutar el respaldo..');
    }
}
