<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class PersonalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personal = User::orderBy('created_at', 'desc')->get();
        \LogActivity::addToLog([
            'modulo'        => 'personal',
            'accion'        => 'listar',
            'descripcion'   => 'Se ha listado Personal',
        ]);

        return view('personal.index', ['personal' => $personal]);
    }

    public function edit($id)
    {
        $usuario = User::find($id);
        \LogActivity::addToLog([
            'modulo'        => 'personal',
            'accion'        => 'visualizar',
            'descripcion'   => 'Se ha visualizado el editor de Personal',
        ], $usuario->toArray());

        return view('personal.edit', ['usuario' => $usuario]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'email'        => 'required|email|max:255',
            'nombre'       => 'required|max:255',
            'cedula'       => 'required|numeric|max:40000000',
            'especialidad' => 'required|max:40',
            'cargo'        => 'required|max:100',
            'telefono'     => 'required|max:15',
            'tipo'         => 'required',
        ]);

        $actualizacion = [
            'name'         => $request->nombre,
            'cedula'       => $request->cedula,
            'especialidad' => $request->especialidad,
            'cargo'        => $request->cargo,
            'telefono'     => $request->telefono,
            'tipo'         => $request->tipo,
        ];

        if (!empty($request->password)) {
            $actualizacion['password'] = bcrypt($request->password);
        }

        $user = User::find($id);
        $mensaje = '';
        if ($user->email !== $request->email) {
            if (User::where('email', $request->email)->first() === null) {
                $actualizacion['email'] = $request->email;
            } else {
                // si el correo esta duplicado no se cambia y se le dice al usuario
                $mensaje = " \nEl correo no fue cambiado por que ya esta registrado con otro usuario.";
            }
        }

        $user->update($actualizacion);
        \LogActivity::addToLog([
            'modulo'        => 'personal',
            'accion'        => 'editar',
            'descripcion'   => 'Se ha editado el Personal'.$request->nombre,
        ], $actualizacion);

        return redirect()->back()->with('success', 'Usuario Actualizado'.$mensaje);
    }

    public function create()
    {
        \LogActivity::addToLog([
            'modulo'        => 'personal',
            'accion'        => 'crear',
            'descripcion'   => 'Se ingresado al formualrio de creacion de personal',
        ]);

        return view('personal.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'email'        => 'required|unique:users|max:255',
            'nombre'       => 'required|max:100',
            'cedula'       => 'required|numeric|max:40000000',
            'especialidad' => 'required|max:100',
            'cargo'        => 'required|max:100',
            'telefono'     => 'required|max:15',
            'tipo'         => 'required',
            'password'     => 'required|max:255',
        ]);

        $user = new User();
        $user->name = $request->nombre;
        $user->cedula = $request->cedula;
        $user->especialidad = $request->especialidad;
        $user->cargo = $request->cargo;
        $user->telefono = $request->telefono;
        $user->tipo = $request->tipo;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        \LogActivity::addToLog([
            'modulo'        => 'personal',
            'accion'        => 'insertar',
            'descripcion'   => 'Se ha insertado un nuevo personal',
        ], [
            'nombre'       => $request->nombre,
            'cedula'       => $request->cedula,
            'especialidad' => $request->especialidad,
        ]);

        return redirect()->route('personal_editar', ['id' => $user->id])->with('success', 'Personal Creado exitosamente');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        \LogActivity::addToLog([
            'modulo'        => 'personal',
            'accion'        => 'eliminar',
            'descripcion'   => 'Se ha eliminado un proyecto',
        ], $user->toArray());
        $user->delete();

        return redirect()->route('personal')->with('info', 'Personal Eliminado Exitosamente');
    }

    public function getTutores(Request $request)
    {
        $request->validate([
            '_type'      => 'required',
        ]);
        $result = [];
        $result['results'] = [];
        if ($request->term) {
            $tutores = User::where('cedula', 'like', '%'.$request->term.'%')
                ->orWhere('name', 'like', '%'.$request->term.'%')
                ->orWhere('especialidad', 'like', '%'.$request->term.'%')
                ->orWhere('email', 'like', '%'.$request->term.'%')
                ->orderBy('created_at', 'desc')->take(50)->get();
            if ($tutores) {
                foreach ($tutores as $tutor) {
                    $result['results'][] = [
                        'id'   => $tutor->id,
                        'text' => '['.$tutor->cedula.'] '.$tutor->name,
                        'name' => $tutor->name,
                    ];
                }
            }
        } else {
            $tutores = User::orderBy('created_at', 'desc')->take(50)->get();
            if ($tutores) {
                foreach ($tutores as $tutor) {
                    $result['results'][] = [
                        'id'   => $tutor->id,
                        'text' => '['.$tutor->cedula.'] '.$tutor->name,
                        'name' => $tutor->name,
                    ];
                }
            }
        }

        return $result;
    }
}
