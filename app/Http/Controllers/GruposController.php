<?php

namespace App\Http\Controllers;

use App\Grupo;
use App\GrupoEstudiante;
use App\Proyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GruposController extends Controller
{
    /**
     * @param $proyecto_id
     *
     * @return mixed
     */
    public function createGroup($proyecto_id)
    {
        $grupo = new Grupo();
        $grupo->proyecto_id = $proyecto_id;
        $grupo->save();
        \LogActivity::addToLog([
            'modulo'        => 'grupos',
            'accion'        => 'insertar',
            'descripcion'   => 'Se ha creado un nuevo grupo',
        ]);

        return $grupo->id;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function add(Request $request)
    {
        $request->validate([
            'proyecto_id'       => 'required',
            'estudiante_id'     => 'required',
        ]);

        try {

        /** Validar si el estudiante ya esta en un grupo */
            $hasGrupo = GrupoEstudiante::where('estudiante_id', $request->estudiante_id)->first();
            if ($hasGrupo->grupo_id) {
                $grupo = Grupo::find($hasGrupo->grupo_id);
                $proyecto = Proyecto::find($grupo->proyecto_id);

                return redirect()->back()
                 ->with('error', 'El estudiante ya se encuentra en el grupo del proyecto: <a href="'
                     .route('proyectos_editar', ['id' => $proyecto->id])
                    .'">'.$proyecto->nombre.'</a>');
            }
            $grupo = Grupo::where('proyecto_id', $request->proyecto_id)->first();

            if (!$grupo) {
                $grupo_id = $this->createGroup($request->proyecto_id);
            } else {
                $grupo_id = $grupo->id;
            }
            if (GrupoEstudiante::where('grupo_id', $grupo_id)->count() == 6) {
                return redirect()->back()
                ->with('error', 'Se ha superado la cantidad maxima de personas en el Grupo!');
            }

            $addGrupo = new GrupoEstudiante();
            $addGrupo->grupo_id = $grupo_id;
            $addGrupo->estudiante_id = $request->estudiante_id;
            $addGrupo->save();
        } catch (\Illuminate\Database\QueryException $e) {
            $mensaje = 'Debe restaurar la base de datos en un punto consistente debido al error siguiente: ';

            return redirect()->route('mantenimiento.get')->with('error', $mensaje.$e->getMessage());
        }

        \LogActivity::addToLog([
            'modulo'        => 'grupos.estudiante',
            'accion'        => 'insertar',
            'descripcion'   => 'Se ha agrego un miembro al grupo',
        ]);

        return redirect()->back()->with('success', 'Estudiante Agregado al Grupo del Proyecto');
    }

    public function delete($estudiante_id, $proyecto_id)
    {
        try {
            $grupo = Grupo::where('proyecto_id', $proyecto_id)->first();

            GrupoEstudiante::where([
            'grupo_id'      => $grupo->id,
            'estudiante_id' => $estudiante_id,
        ])->delete();
        } catch (\Illuminate\Database\QueryException $e) {
            $mensaje = 'Debe restaurar la base de datos en un punto consistente debido al error siguiente: ';

            return redirect()->route('mantenimiento.get')->with('error', $mensaje.$e->getMessage());
        }

        \LogActivity::addToLog([
            'modulo'        => 'grupos.estudiante',
            'accion'        => 'eliminar',
            'descripcion'   => 'Se ha eliminado un miembro del grupo',
        ], $grupo->toArray());

        return redirect()->back()->with('success', 'Estudiante Eliminado del Grupo de Proyecto');
    }

    public function setTutor(Request $request)
    {
        $request->validate([
            'tutor_id'      => 'required',
            'grupo_id'      => 'required',
        ]);
        Grupo::find($request->grupo_id)->update([
            'tutor_id' => $request->tutor_id,
        ]);

        \LogActivity::addToLog([
            'modulo'        => 'grupos',
            'accion'        => 'editar',
            'descripcion'   => 'Se ha configurado el tutor del Grupo',
        ]);

        return ['ok' => true, 'mensaje' => 'success'];
    }

    public function misGrupos()
    {
        try {
            $proyectos = Grupo::where('tutor_id', Auth::id())
                        ->join('proyectos', 'grupos.proyecto_id', '=', 'proyectos.id')
                        ->select('proyectos.*')
                        ->get();
        } catch (\Illuminate\Database\QueryException $e) {
            $mensaje = 'Debe restaurar la base de datos en un punto consistente debido al error siguiente: ';

            return redirect()->route('mantenimiento.get')->with('error', $mensaje.$e->getMessage());
        }

        return view('proyecto.index', ['proyectos'=> $proyectos]);
    }
}
