<?php

namespace App\Http\Controllers;

use App\Estudiante;
use App\Grupo;
use Illuminate\Http\Request;

class EstudiantesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            \LogActivity::addToLog([
                'modulo'        => 'estudiantes',
                'accion'        => 'listar',
                'descripcion'   => 'Se ha listado los estudiantes',
            ]);
            $estudiantes = Estudiante::orderBy('created_at', 'DESC')->get();
        } catch (\Illuminate\Database\QueryException $e) {
            $mensaje = 'Debe restaurar la base de datos en un punto consistente debido al error siguiente: ';

            return redirect()->route('mantenimiento.get')->with('error', $mensaje.$e->getMessage());
        }

        return view('estudiante.index', ['estudiantes' => $estudiantes]);
    }

    public function edit($id)
    {
        try {
            $estudiante = Estudiante::find($id);
        } catch (\Illuminate\Database\QueryException $e) {
            $mensaje = 'Debe restaurar la base de datos en un punto consistente debido al error siguiente: ';

            return redirect()->route('mantenimiento.get')->with('error', $mensaje.$e->getMessage());
        }

        \LogActivity::addToLog([
            'modulo'        => 'estudiantes',
            'accion'        => 'visualizar',
            'descripcion'   => 'Se ha visualizado el editor de estudiante',
        ], $estudiante->toArray());

        return view('estudiante.edit', ['estudiante' => $estudiante]);
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function searchByCedula(Request $request)
    {
        $request->validate([
            '_type'      => 'required',
        ]);
        $result = [];
        $result['results'] = [];
        if ($request->term) {
            $estudiantes = Estudiante::where('cedula', 'like', '%'.$request->term.'%')
                                        ->orWhere('nombres', 'like', '%'.$request->term.'%')
                                        ->orWhere('apellidos', 'like', '%'.$request->term.'%')
                                        ->orderBy('created_at', 'desc')->take(50)->get();
            if ($estudiantes) {
                foreach ($estudiantes as $estudiante) {
                    $result['results'][] = [
                            'id'   => $estudiante->id,
                            'text' => '['.$estudiante->cedula.'] '.$estudiante->nombres.' '.$estudiante->apellidos,
                        ];
                }
            }
        } else {
            $estudiantes = Estudiante::orderBy('created_at', 'desc')->take(50)->get();
            if ($estudiantes) {
                foreach ($estudiantes as $estudiante) {
                    $result['results'][] = [
                        'id'   => $estudiante->id,
                        'text' => '['.$estudiante->cedula.'] '.$estudiante->nombres.' '.$estudiante->apellidos,
                    ];
                }
            }
        }

        return $result;
    }

    public function delete($id)
    {
        $estudiante = Estudiante::find($id);
        $estudiante->delete();

        return redirect()->back()->with('success', 'Eliminado exitosamente!');
    }

    public function get(Request $request)
    {
        $request->validate([
            'cedula'      => 'required',
        ]);

        try {
            $estudiante = Estudiante::where('cedula', $request->cedula)
                                ->join('grupo_estudiante', 'estudiantes.id', '=', 'grupo_estudiante.estudiante_id')
                                ->select('estudiantes.*', 'grupo_estudiante.grupo_id as grupo_id')
                                ->first();
            $tutor = Grupo::where('grupos.id', $estudiante->grupo_id)
                        ->join('users', 'grupos.tutor_id', '=', 'users.id')
                        ->select('grupos.*', 'users.name as tutor_name')
                        ->first();
        } catch (\Illuminate\Database\QueryException $e) {
            $mensaje = 'Debe restaurar la base de datos en un punto consistente debido al error siguiente: ';

            return redirect()->route('mantenimiento.get')->with('error', $mensaje.$e->getMessage());
        }

        $hasTutor = false;
        if ($tutor) {
            $hasTutor = true;
        }
        if ($estudiante) {
            return response(['ok' => true, 'encontrado' => true, 'tutor' => $hasTutor], 200);
        }

        return  response(['ok' => false, 'encontrado' => false], 503);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nombres'      => 'required|max:255',
            'apellidos'    => 'required|max:255',
            'cedula'       => 'required|numeric|max:40000000',
            'carrera'      => 'required|max:100',
            'semestre'     => 'required|numeric',
            'seccion'      => 'required|max:10',
            'telefono'     => 'required|numeric',
            'direccion'    => 'required|max:255',
            'turno'        => 'required',
            'periodo'      => 'required',
        ]);

        $actualizacion = [
            'nombres'   => $request->nombres,
            'apellidos' => $request->apellidos,
            'cedula'    => $request->cedula,
            'carrera'   => $request->carrera,
            'semestre'  => $request->semestre,
            'seccion'   => $request->seccion,
            'telefono'  => $request->telefono,
            'direccion' => $request->direccion,
            'turno'     => $request->turno,
            'periodo'   => $request->periodo,
        ];

        try {
            $user = Estudiante::find($id);
            $mensaje = '';
            if ($user->correo !== $request->correo) {
                if (Estudiante::where('correo', $request->correo)->first() === null) {
                    $actualizacion['correo'] = $request->correo;
                } else {
                    // si el correo esta duplicado no se cambia y se le dice al usuario
                    $mensaje = " \nEl correo no fue cambiado por que ya esta registrado con otro usuario.";
                }
            }

            $user->update($actualizacion);
        } catch (\Illuminate\Database\QueryException $e) {
            $mensaje = 'Debe restaurar la base de datos en un punto consistente debido al error siguiente: ';

            return redirect()->route('mantenimiento.get')->with('error', $mensaje.$e->getMessage());
        }

        \LogActivity::addToLog([
            'modulo'        => 'estudiantes',
            'accion'        => 'editar',
            'descripcion'   => 'Se ha editado el estudiante '.$request->nombres,
        ], $actualizacion);

        return redirect()->back()->with('success', 'Usuario Actualizado'.$mensaje);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombres'      => 'required|max:255',
            'apellidos'    => 'required|max:255',
            'cedula'       => 'required|numeric|max:40000000',
            'carrera'      => 'required|max:100',
            'semestre'     => 'required|numeric',
            'seccion'      => 'required|max:10',
            'telefono'     => 'required|numeric',
            'direccion'    => 'required|max:255',
            'turno'        => 'required',
            'periodo'      => 'required',
            'correo'       => 'required|email|unique:estudiantes',
        ]);

        $estudiante = new Estudiante();
        $estudiante->nombres = $request->nombres;
        $estudiante->apellidos = $request->apellidos;
        $estudiante->cedula = $request->cedula;
        $estudiante->carrera = $request->carrera;
        $estudiante->semestre = $request->semestre;
        $estudiante->seccion = $request->seccion;
        $estudiante->telefono = $request->telefono;
        $estudiante->direccion = $request->direccion;
        $estudiante->turno = $request->turno;
        $estudiante->periodo = $request->periodo;
        $estudiante->correo = $request->correo;
        $estudiante->save();

        \LogActivity::addToLog([
            'modulo'        => 'estudiantes',
            'accion'        => 'insertar',
            'descripcion'   => 'Se ha insertado un estudiante',
        ]);

        return redirect()->back()->with('pdf', base64_encode($estudiante->id));
    }

    public function pdf($id)
    {
        $id = base64_decode($id);
        $estudiante = Estudiante::find($id);
        $html = view('estudiante.pdf.planilla', [
            'estudiante' => $estudiante,
        ]);
        $html = str_replace("\n", '', $html);
        $html = str_replace("\r", '', $html);

        $pdf = \PDF::loadHTML($html);

        return $pdf->stream();
    }

    public function search(Request $request)
    {
        $request->validate([
            'cedula'       => 'required|numeric|max:40000000',
            'correo'       => 'required|email',
        ]);

        $estudiante = Estudiante::where([
            'cedula' => $request->cedula,
            'correo' => $request->correo,

        ])->first();

        if ($estudiante) {
            return redirect()->route('planilla.pdf', [
                    'id' => base64_encode($estudiante->id),
            ]);
        } else {
            return redirect()->back()->with('error', 'Informacion Invalida');
        }
    }
}
