<?php

namespace App\Helpers;

use App\LogActivity as LogActivityModel;
use Request;

class LogActivity
{
    public static function addToLog($datos, $propiedades = [])
    {
        $log = [];
        $log['modulo'] = $datos['modulo'];
        $log['accion'] = $datos['accion'];
        $log['descripcion'] = $datos['descripcion'];
        //Request::fullUrl();
        if (count($propiedades) > 0) {
            // Request::method();
            $log['propiedades'] = json_encode($propiedades);
        }

        $log['ip'] = Request::ip();
        $log['agente'] = Request::header('user-agent');
        $log['user_id'] = auth()->check() ? auth()->user()->id : null;

        LogActivityModel::create($log);
    }

    public static function logActivityLists()
    {
        return LogActivityModel::latest()->get();
    }

    public static function getByUserId($id)
    {
        return LogActivityModel::where('user_id', $id)
                                ->orderBy('id', 'desc')
                                ->get();
    }
}
