<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            // Begin a transaction
            DB::beginTransaction();

            DB::table('users')->insert([
                'name'         => 'Angel Mora',
                'email'        => 'coordsc@gmail.com',
                'password'     => bcrypt('secreto'),
                'cedula'       => 12345678,
                'especialidad' => 'Sociologia',
                'cargo'        => 'Coordinador Servicio Comunitario',
                'telefono'     => '04160253340',
                'tipo'         => 'COORDINADOR',
            ]);

            DB::table('users')->insert([
                'name'         => 'Alexander Rodriguez',
                'email'        => 'alexr1712@gmail.com',
                'password'     => bcrypt('secreto'),
                'cedula'       => 24400007,
                'especialidad' => 'Sistemas',
                'cargo'        => 'Docente',
                'telefono'     => '04160253340',
                'tipo'         => 'TUTOR',
            ]);

            DB::table('users')->insert([
                'name'         => 'Wilmarc Colmenares',
                'email'        => 'wilmarc@gmail.com',
                'password'     => bcrypt('secreto'),
                'cedula'       => 454545554,
                'especialidad' => 'Sistemas',
                'cargo'        => 'Docente',
                'telefono'     => '04160253341',
                'tipo'         => 'TUTOR',
            ]);

            DB::table('users')->insert([
                'name'         => 'Michelle Kheir',
                'email'        => 'michelle@gmail.com',
                'password'     => bcrypt('secreto'),
                'cedula'       => 24658874,
                'especialidad' => 'Sistemas',
                'cargo'        => 'Docente',
                'telefono'     => '04160253342',
                'tipo'         => 'TUTOR',
            ]);

            DB::table('users')->insert([
                'name'         => 'Wilmara Camacho',
                'email'        => 'Wilmara@gmail.com',
                'password'     => bcrypt('secreto'),
                'cedula'       => 24658874,
                'especialidad' => 'Sistemas',
                'cargo'        => 'Docente',
                'telefono'     => '04160253343',
                'tipo'         => 'TUTOR',
            ]);

            for ($i = 0; $i < 5; $i++) {
                $faker = Faker::create();
                DB::table('users')->insert([
                'name'         => $faker->name,
                'email'        => str_slug($faker->name),
                'password'     => bcrypt('secreto'),
                'cedula'       => 12345678,
                'especialidad' => str_random(10),
                'cargo'        => 'Docente',
                'telefono'     => '04160253340',
                'tipo'         => 'TUTOR',
            ]);
            }

            // Commit the transaction
            DB::commit();
        } catch (\Exception $e) {
            // An error occured; cancel the transaction...
            DB::rollback();

            // and throw the error again.
            throw $e;
        }
    }
}
