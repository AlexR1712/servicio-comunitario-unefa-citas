<!DOCTYPE html>
<html lang="es">
<head>
    <title>Administración - Servicio Comunitario</title>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/sweet-alert.css">
    <link rel="stylesheet" href="/css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">
    {{-- <link rel="stylesheet" href="/css/jquery.mCustomScrollbar.css"> --}}
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/select2.min.css">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/moment.js"></script>
    <script src="/js/select2.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables.bootstrap.min.js"></script>
    {{-- <script src="/js/jquery.mCustomScrollbar.concat.min.js"></script> --}}
    <script src="/js/schedule.js"></script>
    <script src="/js/sweet-alert.min.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/bootstrap-datetimepicker.min.js"></script>

	
	<style>
		@import url(http://fonts.googleapis.com/css?family=Imprima);
		*{
			font-family:Imprima,sans-serif;
			font-size:16px;
		}	
	</style>
</head>

<body>
<div class="row" style="background: white none repeat scroll 0% 0%;"><div class="col-md-12"><img src="{{asset('/imagenes/banner.png')}}" class="center-block"></div></div>    
    <div class="navbar-lateral full-reset">
        <div class="visible-xs font-movile-menu mobile-menu-button"></div>
        <div class="full-reset container-menu-movile custom-scroll-containers">
            <div class="logo full-reset all-tittles">
                <i class="visible-xs zmdi zmdi-close pull-left mobile-menu-button" style="line-height: 55px; cursor: pointer; padding: 0 10px; margin-left: 7px;"></i> 
              Panel {{ucwords(strtolower(Auth::user()->tipo))}}
            </div>
            <div class="full-reset" style="background-color:#FFFFF; padding: 10px 0; ">
                <figure>
                    <img src="/assets/img/logo1.gif" class="img-responsive center-box" style="width:60%;">
                </figure>
            </div>
            <div class="full-reset nav-lateral-list-menu">
                <ul class="list-unstyled">
                    @if(Auth::user()->tipo == 'COORDINADOR')
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-assignment"></i>&nbsp;&nbsp; Usuarios/Personal <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="/personal/create"><i class="zmdi zmdi-widgets"></i>&nbsp;&nbsp; Crear Nuevo </a></li>
                            <li><a href="/personal"><i class="zmdi zmdi-male-female"></i>&nbsp;&nbsp; Listado del Personal </a></li>
                           
                        </ul>
                    </li>
                    @endif
                    @if(Auth::user()->tipo == 'COORDINADOR')
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-assignment"></i>&nbsp;&nbsp; Estudiantes <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            {{-- <li><a href="/estudiantes/create"><i class="zmdi zmdi-widgets"></i>&nbsp;&nbsp; Crear Nuevo </a></li> --}}
                            <li><a href="/estudiantes"><i class="zmdi zmdi-accounts"></i>&nbsp;&nbsp; Listado de Estudiantes </a></li>
                        </ul>
                    </li>                                       
                    <li>
                        <div class="dropdown-menu-button"><i class="zmdi zmdi-assignment"></i>&nbsp;&nbsp; Proyectos <i class="zmdi zmdi-chevron-down pull-right zmdi-hc-fw"></i></div>
                        <ul class="list-unstyled">
                            <li><a href="{{route('proyectos_create')}}"><i class="zmdi zmdi-accounts"></i>&nbsp;&nbsp; Crear Nuevo </a></li>
                            <li><a href="{{route('proyectos')}}"><i class="zmdi zmdi-widgets"></i>&nbsp;&nbsp; Listado de Proyectos </a></li>                   
                        </ul>
                    </li>
                    
                    <li><a href="/mensajes"><i class="zmdi zmdi-email"></i>&nbsp;&nbsp; Mensajes </a></li>
                    @endif
                    <li><a href="{{route('citas.list')}}"><i class="zmdi zmdi-time"></i>&nbsp;&nbsp; Citas </a></li>
                        @if(Auth::user()->tipo == 'COORDINADOR')
                    <li><a href="/logActivity"><i class="zmdi zmdi-accounts-list-alt"></i>&nbsp;&nbsp; Bitácora</a></li>
                    <li><a href="/mantenimiento"><i class="zmdi zmdi-wrench"></i>&nbsp;&nbsp; Mantenimiento</a></li>
                            @endif
                    
                            
                            @if(Auth::user()->tipo == 'TUTOR')
                            <li><a href="{{route('personal.grupos.list')}}"><i class="zmdi zmdi-accounts"></i>&nbsp;&nbsp; Mis Grupos </a></li>
                        @endif
                        <li><a href="{{route('personal.horario.show')}}"><i class="zmdi zmdi-assignment"></i>&nbsp;&nbsp; Mi Horario </a></li>

                </ul>
            </div>
        </div>
    </div>
	
    <div class="content-page-container full-reset custom-scroll-containers">
        <nav class="navbar-user-top full-reset">
            <ul class="list-unstyled full-reset">
                <figure>
                   <img src="/assets/img/user04.png" alt="user-picture" class="img-responsive img-circle center-box">
                </figure>
                <li style="color:#fff; cursor:default;">
                    <span class="all-tittles">{{Auth::user()->name}}</span>
                </li>
                <li  class="tooltips-general exit-system-button" 
                 data-href="#" data-placement="bottom" title="Salir del sistema">
                    <i class="zmdi zmdi-power"></i>
                </li>
                <li class="mobile-menu-button visible-xs" style="float: left !important;">
                    <i class="zmdi zmdi-menu"></i>
                </li>
            </ul>
        </nav>
        <div class="container">
            <div class="page-header">
              <h1 class="all-tittles ">Servicio Comunitario <small>@yield('titulo')</small></h1>
            </div>
        </div>
		
		<!--contendio interno-->
		<div style="min-height: 400px;">
            @yield('content')
        </div>       
		
		
        <footer class="footer full-reset">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h4 class="all-tittles">Desarrolladores</h4>
                        <ul class="list-unstyled">
                            <li><i class="zmdi zmdi-check zmdi-hc-fw"></i>&nbsp; Desarrollado por Futuros Ingenieros de Sistemas
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright full-reset all-tittles">8D01IS</div>
        </footer>
    </div>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
</body>
</html>