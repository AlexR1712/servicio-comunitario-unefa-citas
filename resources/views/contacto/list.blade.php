@extends('layouts.panel')
@section('titulo') / Listado de Usuarios @endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('info'))
                <div class="alert alert-info">{{ Session::get('info') }}</div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif        
        </div>
        <div class="col-md-12">

                <!-- tabla -->
                @if (isset($mensajes))
                    <table id="tabla" class= "table table-hover" >
                        <thead>
                            <tr >
                                <th>#</th>
                                <th>Asunto</th>
                                <th>Correo</th>
                                <th>Fecha</th>
                                <th></th>
                            </tr>
                        </thead>                    
                        <tbody>

                            @foreach($mensajes as $mensaje)
                            <tr>
                                <td>{{$mensaje->id}}</td>
                                <td>{{$mensaje->asunto}}</td>
                                <td>{{$mensaje->email}}</td>
                                <td>{{\Carbon\Carbon::parse($mensaje->created_at)->format('d/m/Y - h:i')}}</td>
                                <td> 
                                    <a href="{{route('mensajes.show', ['id'=> $mensaje->id])}}" class="btn btn-info btn-sm">Ver Mensaje</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <h1 class="text-center">No existen Usuarios Registrados</h1>
                    @endif                     
                </div>
    </div>
</div>
    <script>
        $(document).ready(function()
        {
            $("#tabla").DataTable(
            {
                "displayLength": 8,
                "ordering": false,
                "bLengthChange": false,
                "pagingType": "full_numbers",
                "language": 
                {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar MENU registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar: ",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": 
                    {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                }
            });
        });
    
    </script>
@endsection
