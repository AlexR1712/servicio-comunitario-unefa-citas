@extends('layouts.app')
@section('titulo') / Crear Nuevo Estudiante @endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif        <div class="panel panel-default">
                <div class="panel-heading">Contactanos</div>
                <div class="panel-body">
                @if (Session::has('error'))
                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                 @endif
                <form action="{{ route("contacto.store") }}" method="post">
                                <div class="form-group">
                                    <label for="email"><i class="zmdi zmdi-folder-star"></i> Correo Electronico: </label>
                                    <input id="email" class="form-control" type="email" name="email" required>
                                </div>
                                <div class="form-group">
                                    <label for="asunto"><i class="zmdi zmdi-folder-star"></i> Asunto: </label>
                                    <input id="asunto" class="form-control" type="text" name="asunto" required>
                                </div>
                
                                <div class="form-group">
                                    <label for="cor"><i class="zmdi zmdi-folder-star"></i> Mensaje</label>
                                    <textarea class="form-control" name="mensaje" id="" cols="30" rows="10"></textarea>
                                </div>                               
                                                    
                                    {{ csrf_field() }}
                                    {{ method_field("POST") }}
                                    <button type="submit" class="btn btn-success">Enviar Mensaje</button>
                      </form>
                </div>
            </div>
                
        </div>                  
    </div>
</div>
    
@endsection
