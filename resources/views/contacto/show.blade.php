@extends('layouts.panel')
@section('titulo') / Ver Mensaje @endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('info'))
                <div class="alert alert-info">{{ Session::get('info') }}</div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif        
        </div>
        <div class="col-md-8 col-md-offset-3">
            <div class="panel">
                
                <p><strong>Asunto: </strong> {{$mensaje->asunto}}</p> 
                <p><strong>Correo: </strong> {{$mensaje->email}}</p>
                <p><strong>Fecha: </strong> {{\Carbon\Carbon::parse($mensaje->created_at)->format('d/m/Y - h:i')}}</p> 
                <p><strong>Mensaje: </strong>  <br>{{$mensaje->mensaje}}</p> <br>
                <a class="btn btn-danger" href="{{route('mensajes.destroy', ['id'=>$mensaje->id])}}">Eliminar Mensaje</a>
            </div>

                <!-- tabla -->
        </div>
    </div>
</div>
@endsection
