@extends('layouts.panel')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
           
           <h1 class="text-center">Bienvenido!</h1>
            <p>La mision del Proyecto es facilitar la administracion de los datos que se manejan en el area de Servicio Comunitario para los estudiantes, profesores, y coordinadores, de manera que sirva tambien de fuente de informacion actualizada permitiendo agilizar los procesos que alli se desarrollan. </p>
        </div>
    </div>
</div>
@endsection
