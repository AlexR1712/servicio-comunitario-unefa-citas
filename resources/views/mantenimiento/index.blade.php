@extends('layouts.panel')
@section('titulo') / Mantenimiento @endsection
@section('content')
<div clas="container">
<div clas="row">

<div class="col-md-8 col-md-offset-2">

    @if (Session::has('success'))
        <div class="alert alert-success">{{ Session::get('success') }}</div>
    @endif
        @if (Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#Modal"><i class="zmdi zmdi-plus-circle"></i> Nueva Copia de Seguridad</button>
	@if(isset($backups))
        <table class="table table-bordered">
		<tr>
			<th>#</th>
            <th>Fecha</th>
            <th>Descripcion</th>
            <th>Nombre</th>
			<th>Acción</th>
            
		</tr>
    @foreach($backups as $backup)
		<tr>
			<td>{{$backup->id}}</td>
            <td>{{$backup->created_at}}</td>
            <td>{{$backup->descripcion}}</td>
            <td>{{$backup->ruta}}</td>
			<td><a href="{{route('backup.restaurar', $backup->ruta)}}" class="btn btn-info btn-sm">Recuperar</a></td>
		</tr>
        @endforeach
    </table>
        @else
            <h2 class="text-center">No existen backups</h2>
        @endif
    </div>
    </div>
    
<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form action="{{route('backup.make')}}" method="post">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nueva Copia de Seguridad</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="overflow-y: hidden;">

          <div class="form-group">
            <label for="message-text" class="form-control-label">Descripción:</label>
            <textarea class="form-control" id="message-text" name="descripcion" required></textarea>
          </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-success"><i class="zmdi zmdi-plus-circle"></i> Crear Copia</button>
      </div>
    </div>
        {{ csrf_field() }}
        {{ method_field("POST") }}
    </form>
  </div>
</div>
    
    </div>


  

@endsection