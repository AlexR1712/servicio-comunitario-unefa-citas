<!DOCTYPE html>
<html><head><meta charset="UTF-8"></head><body>
<center>
	<p style="text-align: center;"><b>
REPÚBLICA BOLIVARIANA DE VENEZUELA <br>
MINISTERIO DEL PODER POPULAR PARA LA DEFENSA <br>
UNIVERSIDAD NACIONAL EXPERIMENTAL <br>
POLITÉCNICA DE LA FUERZA ARMADA <br>
COMANDANTE SUPREMO DE LA REVOLUCIÓN <br>
BOLIVARIANA “HUGO RAFAEL CHÁVEZ FRÍAS” <br>
DIVISIÓN DE ASUNTOS SOCIALES Y PARTICIPACIÓN SOCIAL <br>
DIRECCIÓN NACIONAL DE EXTENSIÓN <br>
COORDINACIÓN DE SERVICIO COMUNITARIO <br>
NÚCLEO LARA		
	</b></p>
</center>
<br> <br> 
<span style="position: absolute; top: 22%; right: 8%; width: 3.26cm; height: 2.95cm; border: 2.50pt solid #4f81bd; padding: 0.13cm 0.25cm; background: #ffffff; text-align: center;">
	<br><br>FOTO
</span>
<br><br><br>
<h2 style="text-align: center;">PLANILLA DE DATOS PERSONALES</h2>


<br>
<center>
<table class="tg" style="margin-left: 70px;border-collapse: collapse;border-spacing: 0;">
  <tr>
    <th class="tg-9hbo" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;font-weight: bold;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;vertical-align: top;">NOMBRES:</th>
    <th class="tg-yw4l" style="font-family: Arial, sans-serif;font-size: 14px;font-weight: normal;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;vertical-align: top;">{{strtoupper($estudiante->nombres)}}</th>
  </tr>
  <tr>
    <td class="tg-9hbo" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;font-weight: bold;vertical-align: top;">APELLIDOS:</td>
    <td class="tg-yw4l" style="font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;vertical-align: top;">{{strtoupper($estudiante->apellidos)}}</td>
  </tr>
  <tr>
    <td class="tg-9hbo" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;font-weight: bold;vertical-align: top;">CÉDULA DE IDENTIDAD:</td>
    <td class="tg-yw4l" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;vertical-align: top;">{{strtoupper($estudiante->cedula)}}</td>
  </tr>
  <tr>
    <td class="tg-9hbo" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;font-weight: bold;vertical-align: top;">TELÉFONO DE CONTACTO:</td>
    <td class="tg-yw4l" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;vertical-align: top;">{{strtoupper($estudiante->telefono)}}</td>
  </tr>
  <tr>
    <td class="tg-9hbo" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;font-weight: bold;vertical-align: top;">CORREOS ELECTRÓNICO:</td>
    <td class="tg-yw4l" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;vertical-align: top;">{{strtoupper($estudiante->correo)}}</td>
  </tr>
  <tr>
    <td class="tg-9hbo" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;font-weight: bold;vertical-align: top;">DIRECCIÓN DE HABITACIÓN:</td>
    <td class="tg-yw4l" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;vertical-align: top;">{{strtoupper($estudiante->direccion)}}</td>
  </tr>
  <tr>
    <td class="tg-9hbo" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;font-weight: bold;vertical-align: top;">ESPECIALIDAD:</td>
    <td class="tg-yw4l" style="font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;vertical-align: top;">{{strtoupper($estudiante->carrera)}}</td>
  </tr>
  <tr>
    <td class="tg-9hbo" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;font-weight: bold;vertical-align: top;">SECCIÓN:</td>
    <td class="tg-yw4l" style="font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;vertical-align: top;">{{strtoupper($estudiante->seccion)}}</td>
  </tr>
  <tr>
    <td class="tg-9hbo" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;font-weight: bold;vertical-align: top;">TURNO:</td>
    <td class="tg-yw4l" style="font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;vertical-align: top;">{{strtoupper($estudiante->turno)}}</td>
  </tr>
  <tr>
    <td class="tg-9hbo" style="width: 250px; font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;font-weight: bold;vertical-align: top;">PERIODO:</td>
    <td class="tg-yw4l" style="font-family: Arial, sans-serif;font-size: 14px;padding: 10px 5px;border-style: solid;border-width: 1px;overflow: hidden;word-break: normal;vertical-align: top;">{{strtoupper($estudiante->periodo)}}</td>
  </tr>
</table>
<br>
<br>
<p>______________________<br>
	FIRMA ESTUDIANTE</p>
</center></body></html>