@extends('layouts.panel')
@section('titulo') / Listado de Estudiantes @endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('info'))
                <div class="alert alert-info">{{ Session::get('info') }}</div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif        
        </div>

        <div class="col-md-12">
                <!-- tabla -->
                @if (isset($estudiantes))
                    <table id="tabla" class= "table table-hover" >
                        <thead>
                            <tr >
                                <th>#</th>
                                <th>Nombres y Apellidos</th>
                                <th>Cedula</th>
                                <th>Carrera</th>
                                <th>Semestre</th>
                                <th>Turno</th>
                                <th>Seccion</th>
                                <th>Telefono</th>
                                <th>Correo</th>
                                <th></th>
                            </tr>
                        </thead>                    
                        <tbody>

                            @foreach($estudiantes as $estudiante)
                            <tr>
                                <td>{{$estudiante->id}}</td>
                                <td>{{$estudiante->nombres}} {{$estudiante->apellidos}}</td>
                                <td>{{$estudiante->cedula}}</td>
                                <td>{{$estudiante->carrera}}</td>
                                <td>{{$estudiante->semestre}}</td>
                                <td>{{$estudiante->turno}}</td>
                                <td>{{$estudiante->seccion}}</td>
                                <td>{{$estudiante->telefono}}</td>
                                <td>{{$estudiante->correo}}</td>
                                <td> 
                                    <a href="{{route('estudiantes_edit', ['id'=> $estudiante->id ])}}" class="btn btn-success btn-sm">Editar</a>
                                </td>
                                <td> 
                                    <a href="{{route('estudiantes_delete', ['id'=> $estudiante->id ])}}" class="btn btn-danger btn-sm">Eliminar</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <h1 class="text-center">No existen Estudiantes Registrados</h1>
                    @endif                     
                </div>
    </div>
</div>
    <script>
        $(document).ready(function()
        {
            $("#tabla").DataTable(
            {
                "displayLength": 8,
                "ordering": false,
                "bLengthChange": false,
                "pagingType": "full_numbers",
                "language": 
                {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar MENU registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar: ",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": 
                    {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                }
            });
        });
    
    </script>
@endsection
