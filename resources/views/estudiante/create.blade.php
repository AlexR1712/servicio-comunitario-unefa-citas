@extends('layouts.app')
@section('titulo') / Crear Nuevo Estudiante @endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-3">
        @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif        <div class="panel panel-default">
                <div class="panel-heading">Registro de Estudiantes Servicio Comunitario</div>
                <div class="panel-body">
                @if (Session::has('pdf'))
                <div class="alert alert-success">Se ha generado exitosamente su Planilla de Datos Personales de Secretaría</div>
                <br>
                    <embed src="{{route('planilla.pdf', ['id' => Session::get('pdf')])}}" width="100%" height="700" alt="pdf" pluginspage="http://www.adobe.com/products/acrobat/readstep2.html">
                @else
                <form action="{{ route("estudiantes_store") }}" method="post">
                
                                <div class="form-group">
                                    <label for="name"><i class="zmdi zmdi-folder-star"></i> Nombres</label>
                                    <input id="name" class="form-control" type="text" name="nombres"  required>
                                </div>
              
                                <div class="form-group">
                                    <label for="namea"><i class="zmdi zmdi-folder-star"></i> Apellidos</label>
                                    <input id="namea" class="form-control" type="text" name="apellidos" required>
                                </div>
                
                                <div class="form-group">
                                    <label for="cedula"><i class="zmdi zmdi-folder-star"></i> Cédula</label>
                                    <input id="cedula" class="form-control" type="text" name="cedula" onkeypress="return valida(event)" maxlength="10" required>
                                </div>
                
                                <div class="form-group">
                                    <label for="carr"><i class="zmdi zmdi-folder-star"></i> Carrera</label>
                                    <input id="carr" class="form-control" maxlength="100" type="text" name="carrera" required>
                                </div>
              
                                <div class="form-group">
                                    <label for="sem"><i class="zmdi zmdi-folder-star"></i> Semestre</label>
                                    <select class="form-control" name="semestre" id="sem" required>
                                        <option selected="" disabled="">Seleccionar</option>
                                        <option value="1">Primero</option>
                                        <option value="2">Segundo</option>
                                        <option value="3">Tercero</option>
                                        <option value="4">Cuarto</option>
                                        <option value="5">Quinto</option>
                                        <option value="6">Sexto</option>
                                        <option value="7">Séptimo</option>
                                        <option value="8">Octavo</option>
                                        <option value="9">Noveno</option>
                                        <option value="10">Décimo</option>
                                    </select>
                                </div>
              
                                <div class="form-group">
                                    <label for="sec"><i class="zmdi zmdi-folder-star"></i> Sección</label>
                                    <input id="sec" class="form-control" type="text" maxlength="10" name="seccion" required>
                                </div>
              
                                <div class="form-group">
                                    <label for="tel"><i class="zmdi zmdi-folder-star"></i> Teléfono</label>
                                    <input id="tel" class="form-control" type="text" maxlength="15" name="telefono" required>
                                </div>
              
                                <div class="form-group">
                                    <label for="cor"><i class="zmdi zmdi-folder-star"></i> Correo Electrónico</label>
                                    <input id="cor" class="form-control" type="email" name="correo"  required>
                                </div>                  
              
                                <div class="form-group">
                                    <label for="dir"><i class="zmdi zmdi-folder-star"></i> Dirección</label>
                                    <input id="dir" class="form-control" type="text" maxlength="255" name="direccion" required>
                                </div>
              
                                <div class="form-group">
                                    <label for="turno"><i class="zmdi zmdi-folder-star"></i> Turno</label>
                                    <select class="form-control" name="turno" id="turno" required>
                                        <option selected="" disabled="">-- Seleccionar --</option>
                                        <option value="DIURNO">DIURNO</option>
                                        <option value="NOCTURNO">NOCTURNO</option>
                                    </select>
                                </div>
              
                                <div class="form-group">
                                    <label for="per"><i class="zmdi zmdi-folder-star"></i> Periodo</label>
                                    <select class="form-control" name="periodo" id="periodo" required>
                                        <option selected="" disabled="">-- Seleccionar --</option>
                                        <option value="{{date("Y")}}-1">{{date("Y")}}-1</option>
                                        <option value="{{date("Y")}}-2">{{date("Y")}}-2</option>
                                    </select>

                                </div>                  
                                                    
                                    {{ csrf_field() }}
                                    {{ method_field("POST") }}
                                    <button type="submit" class="btn btn-success">Guardar</button>
                      </form>
                      @endif
                </div>
            </div>
                
        </div>                  
    </div>
</div>
    
@endsection
