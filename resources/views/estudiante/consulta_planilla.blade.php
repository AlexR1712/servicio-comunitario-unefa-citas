@extends('layouts.app')
@section('titulo') / Crear Nuevo Estudiante @endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-3">
        @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif        <div class="panel panel-default">
                <div class="panel-heading">Consulta Planilla de Datos Personales Servicio Comunitario</div>
                <div class="panel-body">
                @if (Session::has('error'))
                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                 @endif
                <form action="{{ route("planilla.search") }}" method="post">
                
                                <div class="form-group">
                                    <label for="cedula"><i class="zmdi zmdi-folder-star"></i> Cedula</label>
                                    <input id="cedula" class="form-control" type="text" name="cedula" required>
                                </div>
                
                                <div class="form-group">
                                    <label for="cor"><i class="zmdi zmdi-folder-star"></i> Correo Electronico</label>
                                    <input id="cor" class="form-control" type="email" name="correo"  required>
                                </div>                               
                                                    
                                    {{ csrf_field() }}
                                    {{ method_field("POST") }}
                                    <button type="submit" class="btn btn-success">Consultar y Descargar</button>
                      </form>
                </div>
            </div>
                
        </div>                  
    </div>
</div>
    
@endsection
