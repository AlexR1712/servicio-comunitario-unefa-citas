@extends('layouts.panel')
@section('titulo') 
/ Crear Nuevo Usuario 
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-3">
        @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 

  <div id="home" class="tab-pane fade in active">
    <form action="{{ route("estudiantes_editar_actualizar", ['id' => $estudiante->id]) }}" method="post">
  
                  <div class="form-group">
                      <label for="name"><i class="zmdi zmdi-folder-star"></i> Nombres</label>
                      <input id="name" class="form-control" type="text" name="nombres" value="{{ $estudiante->nombres }}" required>
                  </div>

                  <div class="form-group">
                      <label for="namea"><i class="zmdi zmdi-folder-star"></i> Apellidos</label>
                      <input id="namea" class="form-control" type="text" name="apellidos" value="{{ $estudiante->apellidos }}" required>
                  </div>
  
                  <div class="form-group">
                      <label for="cedula"><i class="zmdi zmdi-folder-star"></i> Cédula</label>
                      <input id="cedula" class="form-control" type="text" name="cedula" onkeypress="return valida(event)" value="{{ $estudiante->cedula }}" required>
                  </div>
  
                  <div class="form-group">
                      <label for="carr"><i class="zmdi zmdi-folder-star"></i> Carrera</label>
                      <input id="carr" class="form-control" type="text" name="carrera" value="{{$estudiante->carrera}}" required>
                  </div>

                  <div class="form-group">
                      <label for="sem"><i class="zmdi zmdi-folder-star"></i> Semestre</label>
                      <input id="sem" class="form-control" type="number" name="semestre" onkeypress="return valida(event)" value="{{$estudiante->semestre}}" required>
                  </div>

                  <div class="form-group">
                      <label for="sec"><i class="zmdi zmdi-folder-star"></i> Sección</label>
                      <input id="sec" class="form-control" type="text" name="seccion" value="{{$estudiante->seccion}}" required>
                  </div>

                  <div class="form-group">
                      <label for="tel"><i class="zmdi zmdi-folder-star"></i> Teléfono</label>
                      <input id="tel" class="form-control" type="text" name="telefono" value="{{$estudiante->telefono}}" required>
                  </div>

                  <div class="form-group">
                      <label for="cor"><i class="zmdi zmdi-folder-star"></i> Correo Electrónico</label>
                      <input id="cor" class="form-control" type="email" name="correo" value="{{$estudiante->correo}}" required>
                  </div>                  

                  <div class="form-group">
                      <label for="dir"><i class="zmdi zmdi-folder-star"></i> Dirección</label>
                      <input id="dir" class="form-control" type="text" name="direccion" value="{{$estudiante->direccion}}" required>
                  </div>

                  <div class="form-group">
                      <label for="turno"><i class="zmdi zmdi-folder-star"></i> Tipo</label>
                      <select class="form-control" name="turno" id="turno" required>
                          <option 
                           value="DIURNO"
                          @if ($estudiante->turno == "DIURNO") 
                           selected 
                          @endif >DIURNO</option>
                          <option value="NOCTURNO" 
                          @if ($estudiante->turno == "NOCTURNO") 
                          selected 
                          @endif >NOCTURNO</option>
                      </select>
                  </div>

                  <div class="form-group">
                      <label for="per"><i class="zmdi zmdi-folder-star"></i> Periodo</label>
                      <input id="per" class="form-control" type="text" name="periodo" value="{{$estudiante->periodo}}" required>
                  </div>                  
                                      
                      {{ csrf_field() }}
                      {{ method_field("PUT") }}
                      <button type="submit" class="btn btn-success">Guardar</button>
        </form>
  </div>

              
        </div> <!-- ./ col-md-8 -->                 
    </div> <!-- ./row -->
</div> <!-- ./container -->

<script>
function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}
</script>
@endsection
