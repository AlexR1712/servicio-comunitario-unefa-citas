@extends('layouts.panel')
@section('titulo')
    / Mi Horario
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-3">
                @if (Session::has('success'))
                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                        <button class="btn btn-success btn-block btn-horario-save center-block" data-user="{{Auth::id()}}">Guardar Horario</button>
                        <br>
                        <div id="horario"></div>
                        <br>
                        <button class="btn btn-success btn-block btn-horario-save center-block" data-user="{{Auth::id()}}">Guardar Horario</button>



            </div> <!-- ./ col-md-8 -->
        </div> <!-- ./row -->
    </div> <!-- ./container -->

    <script>
        function valida(e){
            tecla = (document.all) ? e.keyCode : e.which;

            //Tecla de retroceso para borrar, siempre la permite
            if (tecla==8){
                return true;
            }

            // Patron de entrada, en este caso solo acepta numeros
            patron =/[0-9]/;
            tecla_final = String.fromCharCode(tecla);
            return patron.test(tecla_final);
        }
    </script>

    <script>

        $("#horario").dayScheduleSelector({
// Sun - Sat
            days        : [0, 1, 2, 3, 4, 5, 6],

// HH:mm format
            startTime   : '07:00',

// HH:mm format
            endTime     : '22:30',

// minutes
            interval    : 30,

            stringDays  : ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']
        });

        $("#horario").on('selected.artsy.dayScheduleSelector', function (e, selected) {
            // console.log(e, selected);
        });

        $('.btn-horario-save').click(function() {
            var horarios = $("#horario").data('artsy.dayScheduleSelector').serialize();
            console.log(horarios);

            $.post( "{{route('horarios_store')}}", { user_id: $(this).data('user'), horarios: horarios })
                .done(function( data ) {
                    swal("Horario Guardado!", "Tu horario ahora esta disponible para los estudiantes", "success");
                })
                .fail(function() {
                    alert( "No ha llenado el horario" );
                });
        });

        /* Obtener datos */
        // $("#horario").data('artsy.dayScheduleSelector').serialize();

        /* Insertar Datos */
        /* $("#horario").data('artsy.dayScheduleSelector').deserialize({
            '0': [['09:30', '11:00'], ['13:00', '16:30']]
              });*/

        $.get( "{{route('horarios_show')}}", { user_id: {{Auth::id()}} } )
            .done(function( data ) {
                var horario = JSON.parse(data);
                console.log(horario);
                $("#horario").data('artsy.dayScheduleSelector').deserialize(horario);
            })
            .fail(function() {
                alert( "Fallo al intentar buscar el horario" );
            });;

    </script>



@endsection
