@extends('layouts.panel')
@section('titulo') / Crear Nuevo Proyecto @endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-3">
        @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
                <form action="{{route("proyectos_store")}}" method="post">
                <div class="form-group">
                    <label for="proyecto-name"><i class="zmdi zmdi-folder-star"></i> Titulo Proyecto</label>
                    <input id="proyecto-name" class="form-control" type="text" name="nombre" value="{{old('nombre')}}">
                </div>
                <div class="form-group">
                    <label for="proyecto-status"><i class="zmdi zmdi-dot-circle"></i> Estado Actual</label>
                    <select id="proyecto-status" class="form-control" name="status">
                        <option value="ANTEPROYECTO">ANTEPROYECTO</option>
                        <option value="EJECUCION">EJECUCION</option>
                        <option value="CULMINADO">CULMINADO</option>
                        <option value="RECHAZADO">RECHAZADO</option>
                    </select>
                    </div>
                    <div class="form-group">
                        <label for="proyecto-lugar"><i class="zmdi zmdi-pin"></i> Lugar</label>
                        <input id="proyecto-lugar" class="form-control" type="text" name="lugar" value="{{old('lugar')}}">
                    </div>
                    <div class="form-group">
                        <label for="proyecto-dir"><i class="zmdi zmdi-map"></i> Direccion</label>
                        <input id="proyecto-dir" class="form-control" type="text" name="direccion" value="{{old('direccion')}}">
                    </div>
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <button type="submit" class="btn btn-success">Guardar</button>
                </form>
                
        </div>                  
    </div>
</div>
    
@endsection
