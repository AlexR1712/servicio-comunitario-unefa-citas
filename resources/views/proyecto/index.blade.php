@extends('layouts.panel')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('info'))
                <div class="alert alert-info">{{ Session::get('info') }}</div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif        
        </div>
        <div class="col-md-12">
            @if(Auth::user()->tipo == 'COORDINADOR')
            <a class="btn btn-success pull-left" href="{{route('proyectos_create')}}"><i class="zmdi zmdi-plus-circle"></i> Nuevo Proyecto</a>
            @endif
           {{-- <a class="btn btn-warning pull-left" href="route('proyectos_report') "><i class="zmdi zmdi-file"></i> Reporte PDF</a> --}}
        </div>
        <div class="col-md-12">

                <!-- tabla -->
                @if (isset($proyectos))
                    <table id="tabla" class= "table table-hover" >
                        <thead>
                            <tr >
                                <th>#</th>
                                <th>Proyecto</th>
                                <th>Estatus</th>
                                <th>Lugar</th>
                                <th>Dirección</th>
                                <th>Fecha de Creación</th>
                                <th></th>
                            </tr>
                        </thead>                    
                        <tbody>

                            @foreach($proyectos as $proyecto)
                            <tr>
                                <td>{{$proyecto->id}}</td>
                                <td>{{$proyecto->nombre}}</td>
                                <td>
                                @if ($proyecto->status == "ANTEPROYECTO")
                                    <span class="label label-default">ANTEPROYECTO</span>
                                @elseif ($proyecto->status == "EJECUCIÓN")
                                <span class="label label-primary">EJECUCIÓN</span>
                                @elseif ($proyecto->status == "CULMINADO")
                                <span class="label label-success">CULMINADO</span>
                                @elseif ($proyecto->status == "RECHAZADO")
                                <span class="label label-danger">RECHAZADO</span>
                                @endif </td>
                                <td>{{$proyecto->lugar}}</td>
                                <td>{{$proyecto->direccion}}</td>
                                <td>{{\Carbon\Carbon::parse($proyecto->created_at)->format('d/m/Y')}}</td>
                                <td> 
                                    <a href="{{route('proyectos_editar', ['id'=> $proyecto->id ])}}" class="btn btn-success btn-sm">Editar</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <h1 class="text-center">No existen Proyectos Registrados</h1>
                    @endif                     
                </div>
    </div>
</div>
    <script>
        $(document).ready(function()
        {
            $("#tabla").DataTable(
            {
                "displayLength": 8,
                "ordering": false,
                "bLengthChange": false,
                "pagingType": "full_numbers",
                "language": 
                {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar MENU registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar: ",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": 
                    {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                }/*,
                "aoColumns": 
                [ 
                    {"bSearchable": true}, 
                    {"bSearchable": true}, 
                    {"bSearchable": true}, 
                    {"bSearchable": true},
                    {"bSearchable": false}
                ]*/
            });
        });
    
    </script>
@endsection
