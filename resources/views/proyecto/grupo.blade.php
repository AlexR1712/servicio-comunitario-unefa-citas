@if ($grupo->count() > 0)
    <hr>
    <h3 class="text-left">Prestadores de Servicio</h3>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Nombres y Apellidos</th>
            <th>Cedula</th>
            <th>Telefono</th>
            <th>Email</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($grupo as $estudiante)
        <tr>
            <td>{{$estudiante->nombres}} {{$estudiante->apellidos}}</td>
            <td>{{$estudiante->cedula}}</td>
            <td>{{$estudiante->telefono}}</td>
            <td>{{$estudiante->correo}}</td>
            <td><a href="{{route('grupo.delete', [
                'estudiante_id' => $estudiante->id,
                'proyecto_id' => $proyecto->id
            ])}}" class="btn btn-danger btn-xs">Eliminar</a></td>
        </tr>
        @if ($loop->last)
            @php $grupo_id = $estudiante->grupo_id; @endphp
        @endif
        @endforeach
        </tbody>
    </table>
@else
    <div class="alert alert-warning">
        <strong>Informacion</strong> No existen estudiantes para este grupo de Proyecto
    </div>
@endif
@if($grupo->count() < 6)
    <form action="{{route('grupo.add')}}" method="post">
        <div class="form-group">
            <label for="estudiante_id">Agregar estudiante al Grupo</label>
           <select id="estudiante_id" class="js-estudiante" name="estudiante_id" style="width: 100%"></select>
        </div>
        {{ csrf_field() }}
        <input type="hidden" name="proyecto_id" value="{{$proyecto->id}}">
        <button type="submit" class="btn btn-success">Añadir al Grupo</button>
    </form>
@else
    <div class="alert alert-warning">
        <strong>Informacion</strong> Este grupo a superado la cantidad maxima de prestadores de servicio.
    </div>
@endif
@if($grupo->count() > 0 && Auth::user()->tipo == 'COORDINADOR')
    <hr>
    <h3 class="text-left">Tutor de Grupo</h3>
    <form id="tutorform">
        <div class="form-group">
            <label for="tutor">Tutor de Grupo</label>
            <select id="tutor" class="js-tutor" name="tutor_id" style="width: 100%">
                @if($tutor)
                    <option value="{{$tutor->id}}" selected>[{{$tutor->cedula}}] {{$tutor->name}}</option>
                    @endif
            </select>
            @if(!$tutor)
                <div class="alert alert-warning">
                    <strong>Informacion</strong> Este grupo a no tiene tutor asignado...
                </div>
                @endif
            <input type="hidden" name="grupo_id" id="grupo_id" value="{{$grupo_id}}">
        </div>
    </form>
@endif
<script>
    $('.js-estudiante').select2({
        ajax: {
            url: '{{route('estudiantes_search')}}',
            dataType: 'json'
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
    });

    $('.js-tutor').select2({
        ajax: {
            url: '{{route('tutores.get')}}',
            dataType: 'json'
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        }
    });
    $('.js-tutor').on("select2:select", function (e) {
        console.log(e.params.data)
        swal({
            title: 'Estas seguro de agregar el tutor al grupo?',
            text: "Tutor: "+e.params.data.name,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirmar',
            cancelButtonText: 'Cancelar'
        }).then( function (result) {
            console.log(result)
            if (result.value) {
                // grupo.set.tutor rout
                var datastring = $('#tutorform').serialize();
                $.ajax({
                    type: "POST",
                    url: "{{route('grupo.set.tutor')}}",
                    data: datastring,
                    dataType: "json",
                    success: function(data) {
                        swal(
                            'Procesado con exito!',
                            'Tutor Cambiado',
                            'success'
                        )
                    },
                    error: function() {
                        alert('Ha ocurrido un error...');
                    }
                });
            }
        });
    });
</script>


