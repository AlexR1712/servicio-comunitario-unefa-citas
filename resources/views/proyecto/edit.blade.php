@extends('layouts.panel')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-3">
        @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{!! Session::get('error') !!}</div>
            @endif

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Datos Proyecto</a></li>
  <li><a data-toggle="tab" href="#menu1">Grupo Proyecto</a></li>
</ul>        

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
                <form action="{{route("proyectos_editar_actualizar", ['id' => $proyecto->id])}}" method="post">
                <div class="form-group">
                    <label for="proyecto-name"><i class="zmdi zmdi-folder-star"></i> Titulo Proyecto</label>
                    <input id="proyecto-name" class="form-control" type="text" name="nombre" value="{{$proyecto->nombre}}">
                </div>
                <div class="form-group">
                    <label for="proyecto-status"><i class="zmdi zmdi-dot-circle"></i> Estado Actual</label>
                    <select id="proyecto-status" class="form-control" name="status">
                        <option value="ANTEPROYECTO" 
                        @if ($proyecto->status == "ANTEPROYECTO") 
                            selected
                        @endif >ANTEPROYECTO</option>
                        <option value="EJECUCION" 
                        @if ($proyecto->status == "EJECUCION") 
                            selected
                        @endif >EJECUCION</option>
                        <option value="CULMINADO"                         
                        @if ($proyecto->status == "CULMINADO") 
                            selected
                        @endif >CULMINADO</option>
                        <option value="RECHAZADO" 
                        @if ($proyecto->status == "RECHAZADO") 
                            selected
                        @endif >RECHAZADO</option>
                    </select>
                    </div>
                    <div class="form-group">
                        <label for="proyecto-lugar"><i class="zmdi zmdi-pin"></i> Lugar</label>
                        <input id="proyecto-lugar" class="form-control" type="text" name="lugar" value="{{$proyecto->lugar}}">
                    </div>
                    <div class="form-group">
                        <label for="proyecto-dir"><i class="zmdi zmdi-map"></i> Direccion</label>
                        <input id="proyecto-dir" class="form-control" type="text" name="direccion" value="{{$proyecto->direccion}}">
                    </div>
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <button type="submit" class="btn btn-success">Guardar</button><a href="{{route('proyectos_delete', ['íd' => $proyecto->id])}}" class="btn btn-danger">Eliminar</a>
                </form>
            </div>
            <div id="menu1" class="tab-pane fade in">
                @include('proyecto.grupo')
            </div>

            </div>
        </div>
                
        </div>                  
    </div>
    
@endsection
