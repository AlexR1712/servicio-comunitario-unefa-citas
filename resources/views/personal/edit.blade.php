@extends('layouts.panel')
@section('titulo') 
/ Crear Nuevo Usuario 
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-3">
        @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif 

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">Perfil de Usuario</a></li>
  <li><a data-toggle="tab" href="#menu1">Horario</a></li>
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <form action="{{ route("personal_editar_actualizar", ['id' => $usuario->id]) }}" method="post">
  
                  <div class="form-group">
                      <label for="name"><i class="zmdi zmdi-folder-star"></i> Nombre</label>
                      <input id="name" class="form-control" type="text" name="nombre" value="{{ $usuario->name }}" required>
                  </div>
  
                  <div class="form-group">
                      <label for="cedula"><i class="zmdi zmdi-folder-star" ></i> Cedula</label>
                      <input id="cedula" class="form-control" type="text" name="cedula" onkeypress="return valida(event)" value="{{ $usuario->cedula }}" maxlength="10" required>
                  </div>
  
                  <div class="form-group">
                      <label for="Especialidad"><i class="zmdi zmdi-folder-star"></i> Especialidad</label>
                      <input id="Especialidad" class="form-control" type="text" name="especialidad" value="{{$usuario->especialidad}}" required>
                  </div>
  
                  <div class="form-group">
                      <label for="Cargo"><i class="zmdi zmdi-folder-star"></i> Cargo</label>
                      <input id="Cargo" class="form-control" type="text" name="cargo" value="{{$usuario->cargo}}" required>
                  </div>  
  
                  <div class="form-group">
                      <label for="Telefono"><i class="zmdi zmdi-folder-star"></i> Telefono</label>
                      <input id="Telefono" class="form-control" type="text" name="telefono" value="{{$usuario->telefono}}" required>
                  </div> 
  
                  <div class="form-group">
                      <label for="Tipo"><i class="zmdi zmdi-folder-star"></i> Tipo</label>
                      <select class="form-control" name="tipo" id="Tipo" required>
                          <option 
                           value="TUTOR"
                          @if ($usuario->tipo == "TUTOR") 
                           selected 
                          @endif >TUTOR</option>
                          <option value="COORDINADOR" 
                          @if ($usuario->tipo == "COORDINADOR") 
                          selected 
                          @endif >COORDINADOR</option>
                      </select>
                  </div>
  
                  <div class="form-group">
                      <label for="EMAIL"><i class="zmdi zmdi-folder-star"></i> Correo Electronico</label>
                      <input id="EMAIL" class="form-control" type="email" name="email" value="{{$usuario->email}}" required>
                  </div>  
  
                  <div class="form-group">
                      <label for="clave"><i class="zmdi zmdi-folder-star"></i> Cambiar Contraseña (Opcional)</label>
                      <input id="clave" class="form-control" type="password" name="password">
                  </div>                                                                                    
                      {{ csrf_field() }}
                      {{ method_field("PUT") }}
                      <button type="submit" class="btn btn-success">Guardar</button>
        </form>
  </div>

  <div id="menu1" class="tab-pane fade">
      <br>
    <button class="btn btn-success btn-block btn-horario-save center-block" data-user="{{$usuario->id}}">Guardar Horario</button>
    <br>
    <div id="horario"></div>
      <br>
    <button class="btn btn-success btn-block btn-horario-save center-block" data-user="{{$usuario->id}}">Guardar Horario</button>
  </div>
</div>       
                
                
        </div> <!-- ./ col-md-8 -->                 
    </div> <!-- ./row -->
</div> <!-- ./container -->

<script>
function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}
</script>

<script>

$("#horario").dayScheduleSelector({
// Sun - Sat
days        : [0, 1, 2, 3, 4, 5, 6],  

// HH:mm format
startTime   : '07:00',         

// HH:mm format       
endTime     : '22:30',

// minutes                
interval    : 30,    

stringDays  : ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']
});

$("#horario").on('selected.artsy.dayScheduleSelector', function (e, selected) {
        // console.log(e, selected);
});

$('.btn-horario-save').click(function() {
    var horarios = $("#horario").data('artsy.dayScheduleSelector').serialize();
    console.log(horarios);

    $.post( "{{route('horarios_store')}}", { user_id: $(this).data('user'), horarios: horarios })
    .done(function( data ) {
        swal("Horario Guardado!", "Tu horario ahora esta disponible para los estudiantes", "success");
    })
    .fail(function() {
        alert( "No ha llenado el horario" );
    });
});

/* Obtener datos */
// $("#horario").data('artsy.dayScheduleSelector').serialize();

/* Insertar Datos */
/* $("#horario").data('artsy.dayScheduleSelector').deserialize({
    '0': [['09:30', '11:00'], ['13:00', '16:30']]
      });*/

$.get( "{{route('horarios_show')}}", { user_id: {{$usuario->id}} } )
  .done(function( data ) {
    var horario = JSON.parse(data);
    console.log(horario);
    $("#horario").data('artsy.dayScheduleSelector').deserialize(horario);
  })
  .fail(function() {
        alert( "Fallo al intentar buscar el horario" );
    });;

</script>



@endsection
