@extends('layouts.panel')
@section('titulo') / Listado de Usuarios @endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('info'))
                <div class="alert alert-info">{{ Session::get('info') }}</div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif        
        </div>
        <div class="col-md-12">
            <a class="btn btn-success pull-left" href="{{route('personal_create')}}"><i class="zmdi zmdi-plus-circle"></i> Nuevo Usuario</a>
        </div>
        <div class="col-md-12">

                <!-- tabla -->
                @if (isset($personal))
                    <table id="tabla" class= "table table-hover" >
                        <thead>
                            <tr >
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Cedula</th>
                                <th>Especialidad</th>
                                <th>Cargo</th>
                                <th>Telefono</th>
                                <th>Tipo</th>
                                <th></th>
                            </tr>
                        </thead>                    
                        <tbody>

                            @foreach($personal as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>@if ($user->cedula) {{$user->cedula}} @else DESCONOCIDO @endif</td>
                                <td>@if ($user->especialidad) {{$user->especialidad}} @else DESCONOCIDO @endif</td>
                                <td>@if ($user->cargo) {{$user->cargo}} @else DESCONOCIDO @endif</td>
                                <td>@if ($user->telefono) {{$user->telefono}} @else DESCONOCIDO @endif</td>
                                <td>@if ($user->tipo) {{$user->tipo}} @else DESCONOCIDO @endif</td>
                                <td>@if ($user->email) {{$user->email}} @else DESCONOCIDO @endif</td>
                                <td> 
                                    <a href="{{route('personal_editar', ['id'=> $user->id ])}}" class="btn btn-success btn-sm">Editar</a>
                                </td>
                                <td> 
                                    <a href="{{route('personal_delete', ['id'=> $user->id ])}}" class="btn btn-danger btn-sm">Eliminar</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <h1 class="text-center">No existen Usuarios Registrados</h1>
                    @endif                     
                </div>
    </div>
</div>
    <script>
        $(document).ready(function()
        {
            $("#tabla").DataTable(
            {
                "displayLength": 8,
                "ordering": false,
                "bLengthChange": false,
                "pagingType": "full_numbers",
                "language": 
                {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar MENU registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar: ",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": 
                    {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                }
            });
        });
    
    </script>
@endsection
