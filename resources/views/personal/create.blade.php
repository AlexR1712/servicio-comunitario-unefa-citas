@extends('layouts.panel')
@section('titulo') / Crear Nuevo Usuario @endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-3">
        @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif        
                <form action="{{route("personal_store")}}" method="post">

                <div class="form-group">
                    <label for="name"><i class="zmdi zmdi-folder-star"></i> Nombre</label>
                    <input id="name" class="form-control" type="text" name="nombre" value="{{old('nombre')}}" required>
                </div>

                <div class="form-group">
                    <label for="cedula"><i class="zmdi zmdi-folder-star"></i> Cedula</label>
                    <input id="cedula" class="form-control" type="text" name="cedula" onkeypress="return valida(event)" value="{{old('cedula')}}" required maxlength="10">
                </div>

                <div class="form-group">
                    <label for="Especialidad"><i class="zmdi zmdi-folder-star"></i> Especialidad</label>
                    <input id="Especialidad" class="form-control" type="text" name="especialidad" value="{{old('cedula')}}" required>
                </div>

                <div class="form-group">
                    <label for="Cargo"><i class="zmdi zmdi-folder-star"></i> Cargo</label>
                    <input id="Cargo" class="form-control" type="text" name="cargo" value="{{old('cargo')}}" required>
                </div>  

                <div class="form-group">
                    <label for="Telefono"><i class="zmdi zmdi-folder-star"></i> Telefono</label>
                    <input id="Telefono" class="form-control" type="text" name="telefono" value="{{old('telefono')}}" required>
                </div> 

                <div class="form-group">
                    <label for="Tipo"><i class="zmdi zmdi-folder-star"></i> Tipo</label>
                    <select class="form-control" name="tipo" id="Tipo" required>
                        <option value="TUTOR">TUTOR</option>
                        <option value="COORDINADOR">COORDINADOR</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="EMAIL"><i class="zmdi zmdi-folder-star"></i> Correo Electronico</label>
                    <input id="EMAIL" class="form-control" type="email" name="email" value="{{old('email')}}" required>
                </div>  

                <div class="form-group">
                    <label for="clave"><i class="zmdi zmdi-folder-star"></i> Contraseña</label>
                    <input id="clave" class="form-control" type="password" name="password" required>
                </div>                                                                                    
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <button type="submit" class="btn btn-success">Guardar</button>
                </form>
                
        </div>                  
    </div>
</div>
    <script>
function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}
</script>
@endsection
