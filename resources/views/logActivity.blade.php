@extends('layouts.panel')

@section('content')

<div clas="container">
<div clas="row">

<div class="col-md-12" style="background: white !important;">
	<h1>Bitácora</h1>
	<table class="table" >
		<tr>
			<th>#</th>
            <th>Acción</th>
            <th>Módulo</th>
			<th>Descripción</th>
            <th>IP</th>
            <th>Fecha de Actualización</th>
			
		</tr>
		@if($logs->count())
			@foreach($logs as $key => $log)
			<tr>
				<td>{{ ++$key }}</td>
                <td><label class="label label-info">{{ ucfirst($log->accion) }}</label></td>
				<td>{{ucfirst( $log->modulo) }}</td>
				<td>{{ $log->descripcion }}</td>
				<td class="text-warning">{{ $log->ip }}</td>
				<td>{{ $log->updated_at }}</td>
			</tr>
			@endforeach

		@endif
	</table>
	<div style="margin-bottom: 100px"></div>
    </div>
    </div>
</div>
@endsection
