@extends('layouts.app')
@section('titulo') / Crear Nuevo Estudiante @endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        @if (Session::has('success'))
            <div class="alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif        <div class="panel panel-default">
                <div class="panel-heading">Agendar Cita</div>
                <div class="panel-body">
                @if (Session::has('error'))
                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                 @endif
                <form action="{{ route("citas.store") }}" method="post">
                                <div class="form-group">
                                    <label for="cedula"><i class="zmdi zmdi-folder-star"></i> Cedula de Identidad: </label>
                                    <input id="cedula" class="form-control" type="number" name="cedula" required>
                                </div>

                                <div class="form-group">
                                    <label for="asu"><i class="zmdi zmdi-folder-star"></i> Asunto </label>
                                    <input disabled="" id="asu" class="form-control" type="text" name="asunto" required>
                                </div>

                                <div class="form-group">
                                    <label for="tipo"><i class="zmdi zmdi-folder-star"></i> Cita Dirigida: </label>
                                    <select name="tipo" id="tipo" class="form-control" disabled="">
                                        <option selected="" disabled="">-- Seleccionar --</option>
                                        <option value="TUTOR" disabled="" id="tutoropc">Tutor de Grupo</option>
                                        <option value="COORDINADOR">Coordinador Servicio Comunitario</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="asunto"><i class="zmdi zmdi-folder-star"></i> Fecha de Cita: </label>
                                    <input name="fecha" class="form-control" type="text" id="datetimepicker" disabled=""/>
                                </div>

                                <div class="form-group">
                                    <label for="cor"><i class="zmdi zmdi-folder-star"></i> Bloque de Horas</label>
                                    <select name="hora" id="hora" class="form-control" disabled="">
                                        <option selected="" disabled="">-- Seleccionar --</option>
                                    </select>
                                </div>                               
                                                    
                                    {{ csrf_field() }}
                                    {{ method_field("POST") }}
                                    <button type="submit" class="btn btn-success">Agendar Cita</button>
                      </form>
                </div>
            </div>
                
        </div>                  
    </div>
</div>

<script>
/*
(function($) {
  var today = new Date();
  var lastDate = new Date(today.getFullYear(), today.getMonth() + 1, 0);
    $("#datepicker").datepicker({
        startDate: today,
        endDate: lastDate,
        format: 'dd/mm/yyyy'
    });  
})(jQuery);
*/
$(function () {



var typingTimer;                //timer identifier
var doneTypingInterval = 1500;  //time in ms (5 seconds)

//on keyup, start the countdown
$('#cedula').keyup(function() {
    clearTimeout(typingTimer);
    if ($('#cedula').val()) {
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    }
});

//user is "finished typing," do something
function doneTyping () {

    $.getJSON('{{route('estudiantes.get')}}', {
        cedula: $('#cedula').val().trim()
    })
    .done(function (data) {
        console.log(data)
        if (data.tutor) {
            $('#tutoropc').prop('disabled', false);
        }
        swal('Detalles','Hemos verificado tus datos, ahora procede a llenar los detalles de la cita.','success')
        $('#asu').prop('disabled', false);
        // $('#tipo').prop('disabled', false);
        $('#datetimepicker').prop('disabled', false);
    })
    .fail(function (data){
        swal('Detalles','La cedula indicada no esta registrada en nuestra base de datos..','error')
    });
}

$(document).ready(function () {
    $('#datetimepicker').datepicker({
        format: 'yyyy-mm-dd',
        startDate: '0d'
    });
    $('#datetimepicker').on('changeDate', function() {
        console.log('cambio fecha', this.value);
        var data = {};
        data.tipo   = $('#tipo').val();
        data.fecha  = $('#datetimepicker').datepicker('getFormattedDate');
        data.cedula = $('#cedula').val();

        $.getJSON('{{route('horarios.get')}}', data)
            .done(function (data) {
                $('#hora option').remove();
                console.log(data)
                if (data.horario) {
                    $.each(data.horario, function (i, item) {
                        $('#hora').append($('<option>', {
                            value: item.id,
                            text : `${item.dia} de ${item.inicio} a ${item.fin}`
                        }));
                    });
                    $('#hora').prop('disabled', false);
                } else {
                    $('#hora').prop('disabled', true);
                    swal('Detalles','No existen horarios para el dia solicitado','info')
                }

            })
            .fail(function (data){
                swal('Detalles','Ha ocurrido un problema, por favor intente mas tarde...','error')
            });

    });
});
var timerEstu;
$('#asu').keyup(function() {
    clearTimeout(timerEstu);
    if ($('#asu').val().trim()) {
        timerEstu = setTimeout(getEstu, doneTypingInterval)
    } else {
        $('#tipo').prop('disabled', true);
    }
});

function getEstu() {
    $('#tipo').prop('disabled', false);
}
            
});


</script>
@endsection
