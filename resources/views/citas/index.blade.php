@extends('layouts.panel')
@section('titulo') / Listado de Citas @endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('info'))
                <div class="alert alert-info">{{ Session::get('info') }}</div>
            @endif
            @if (Session::has('success'))
                <div class="alert alert-info">{{ Session::get('success') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-danger">{{ Session::get('error') }}</div>
            @endif        
        </div>

        <div class="col-md-12">
                <!-- tabla -->
                @if (isset($citas))
                    <table id="tabla" class= "table table-hover" >
                        <thead>
                            <tr >
                                {{--<th>#</th>--}}
                                <th>Asunto</th>
                                <th>Nombres y Apellidos</th>
                                <th>Cedula</th>
                                <th>Telefono</th>
                                <th>Correo</th>
                                <th>Dia y Hora</th>
                                <th>Fecha Creacion</th>
                                <th></th>
                            </tr>
                        </thead>                    
                        <tbody>

                            @foreach($citas as $cita)
                            <tr>
                               {{-- <td>{{$cita->id}}</td>--}}
                                <td>{{$cita->asunto}}</td>
                                <td>{{$cita->nombres}} {{$cita->apellidos}}</td>
                                <td>{{$cita->cedula}}</td>
                                <td>{{$cita->telefono}}</td>
                                <td>{{$cita->correo}}</td>

                                <td>{{$cita->dia}} de {{$cita->hora_inicio}} a {{$cita->hora_fin}}</td>
                                <td>{{$cita->created_at}}</td>
                                <td> 
                                    {{-- <a href="{{route('estudiantes_edit', ['id'=> $estudiante->id ])}}" class="btn btn-success btn-sm">Editar</a> --}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <h1 class="text-center">No Existen Citas Registradas</h1>
                    @endif                     
                </div>
    </div>
</div>
    <script>
        $(document).ready(function()
        {
            $("#tabla").DataTable(
            {
                "displayLength": 8,
                "ordering": false,
                "bLengthChange": false,
                "pagingType": "full_numbers",
                "language": 
                {
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar MENU registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar: ",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": 
                    {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                }
            });
        });
    
    </script>
@endsection
