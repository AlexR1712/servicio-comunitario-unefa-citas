$(document).ready(function() {
    $('#tabla').DataTable({
        "fnFormatNumber": false,
        "pageLength": 8,
        "lengthChange": false,
        "order":[[0, "asc"]],
            "language": {   
            "zeroRecords": "No se encontraron resultados en su búsqueda",
            "search": "<b>Buscar:</b>",
            "infoEmpty": "No existen registros",
            "infoFiltered": "{filtrada de _MAX_ registros}",
            "paginate": {
	            "first":    "<b>Primero</b>",
	            "last":    "<b>Último</b>",
	            "next":     "<b>Siguiente</b>",
                        "previous":     "<b>Anterior</b>",
	        },
             },
    });
});
